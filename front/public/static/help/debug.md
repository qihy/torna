# 调试接口

如果要调试接口，必须先设置调试环境。前往【模块配置】

<img src="/static/help/images/debug1.png" style="height: 200px" />

找到 【调试环境】，点击添加环境

<img src="/static/help/images/debug2.png" style="height: 100px" />

输入环境名称以及调试路径，到端口号即可

<img src="/static/help/images/debug3.png" style="height: 300px" />

切换到【浏览模式】预览文档，在调试页面会出现刚才配置的调试环境

<img src="/static/help/images/debug4.png" style="height: 200px" />

此外还可以添加测试环境等其它环境，方便在不同环境中进行调试。