package cn.torna.sdk.common;

/**
 * @author tanghc
 */
public class OpenConfig {

    /** 成功返回码值 */
    public static String successCode = "0";
    /** 默认版本号 */
    public static String defaultVersion = "1.0";
    /** 接口属性名 */
    public static String apiName = "name";
    /** 版本号名称 */
    public static String versionName = "version";
    /** appKey名称 */
    public static String appKeyName = "app_key";
    /** data名称 */
    public static String dataName = "data";
    /** 时间戳名称 */
    public static String timestampName = "timestamp";
    /** 时间戳格式 */
    public static String timestampPattern = "yyyy-MM-dd HH:mm:ss";
    /** 签名串名称 */
    public static String signName = "sign";
    /** 格式化名称 */
    public static String formatName = "format";
    /** 格式类型 */
    public static String formatType = "json";
    /** accessToken名称 */
    public static String accessTokenName = "access_token";
    /** 国际化语言 */
    public static String locale = "zh-CN";
    /** 响应code名称 */
    public static String responseCodeName = "code";
    /** 请求超时时间 */
    public static int connectTimeoutSeconds = 10;
    /** http读取超时时间 */
    public static int readTimeoutSeconds = 10;

}
