# 更新日志

## 1.0.1

- [feat]支持docker部署 [#I3AWCB](https://gitee.com/durcframework/torna/issues/I3AWCB)
- [fix]OpenAPI推送无法删除字段
- [fix]进入admin管理页报错问题
- [fix]调试页面跳转模块管理无法设置调试环境问题

## 1.0.0

- 支持接口文档增删改查
- 支持导入外部接口（支持导入swagger、postman）
- 支持OpenAPI管理接口
- 支持字典管理
- 支持导出为markdown格式、html格式
- 支持多环境接口调试
- 支持文档权限管理，访客、开发者、管理员对应不同权限
- 提供`管理模式`和`浏览模式`双模式，管理模式用来编辑文档内容，浏览模式纯粹查阅文档，界面无其它元素干扰