package cn.torna.web.controller.system.vo;

import lombok.Data;

/**
 * @author tanghc
 */
@Data
public class ConfigVO {

    private Boolean enableCaptcha;

}
